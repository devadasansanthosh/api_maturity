import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import Axios from "axios";
import { LinkContainer } from "react-router-bootstrap";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(1),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 405,
  },
}));

export default function Dashboard() {
  const score = useSelector((state) => state.contractModelingScore);
  console.log(score);
  const classes = useStyles();
  const [capabilities, setCapabilities] = React.useState([]);
  const [buttonColor, setButtonColor] = React.useState("btn btn-primary");
  var dbutton1 = "btn btn-primary";

  useEffect(() => {
    Axios.get("http://localhost:8080/api/dimensions").then((response) => {
      const data = response.data;
      const row = data.map((el) => {
        return {
          dimensionID: el.dimensionID,
          dimensionName: el.dimensionName,
          capabilities: el.capabilities,
        };
      });
      setCapabilities(row);
    });
  }, []);
  console.log(capabilities);
  const handleClickOpen = () => {
    console.log("in open");
    //setOpen(true);
  };

  if (score !== null) {
    if (score > 100) {
      dbutton1 = "btn btn-success";
    }
    if (score < 100 && score > 50) {
      dbutton1 = "btn btn-warning";
    }
    if (score > 0 && score < 50) {
      dbutton1 = "btn btn-danger";
    }
  }
  console.log(dbutton1);
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="absolute">
          <h2 className="text-center">IBM API Maturity Assessment</h2>
        </AppBar>

        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Container maxWidth="lg" className={classes.container}>
            <Grid container spacing={3}>
              {capabilities.map((row) => (
                <Grid item xs={12} md={4} lg={5} key={row.dimensionID}>
                  <Paper className={fixedHeightPaper}>
                    <h5
                      style={{
                        padding: 10,
                        textAlign: "center",
                        fontWeight: "bold",
                        fontSize: 20,
                      }}
                    >
                      {row.dimensionName}
                    </h5>
                    {row.capabilities.map((ele) => (
                      <LinkContainer
                        key={ele.capabilityid}
                        style={{ marginBottom: 5 }}
                        to={`/getquestions/${ele.capabilityid}`}
                      >
                        <button
                          type="button"
                          className={buttonColor}
                          style={{
                            padding: 10,
                            justifyItems: "center",
                            marginBottom: 5,
                          }}
                        >
                          {ele.capabilityName}
                        </button>
                      </LinkContainer>
                    ))}
                  </Paper>
                </Grid>
              ))}
            </Grid>
          </Container>
        </main>
      </div>
    </>
  );
}
