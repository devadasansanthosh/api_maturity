import { CONTRACT_MODELING, ADD_LIST_QA } from "../actions/DashBoardActions";
import { combineReducers } from "redux";

function contractModelingScore(state = [], action) {
  switch (action.type) {
    case CONTRACT_MODELING:
      return action.score;
    default:
      return state;
  }
}

export const addQAReducer = (state = { submittedQA: [] }, action) => {
  switch (action.type) {
    case ADD_LIST_QA:
      const quesAns = action.payload;

      return {
        ...state,
        submittedQA: [...state.submittedQA, quesAns],
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({ contractModelingScore, addQAReducer });

export default rootReducer;
