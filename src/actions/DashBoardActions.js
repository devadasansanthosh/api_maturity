export const CONTRACT_MODELING = "CONTRACT_MODELING";
export const ADD_LIST_QA = "ADD_LIST_QA";

export function setContractModelingScore(score) {
  return {
    type: CONTRACT_MODELING,
    score,
  };
}

export const addQandA = (ques, clickedAnswer) => {
  return {
    type: ADD_LIST_QA,
    payload: {
      question: ques,
      answers: clickedAnswer,
    },
  };
};
