import React from "react";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import Dashboard from "./Dashboard";
import ContractModeling from "./components/ContractModeling";

function App() {
  return (
    <BrowserRouter>
      <Route path="/" component={Dashboard} exact></Route>
      <Route
        exact
        path="/getquestions/:capabilityID"
        component={ContractModeling}
      ></Route>
    </BrowserRouter>
  );
}

export default App;
