import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setContractModelingScore,
  addQandA,
} from "../actions/DashBoardActions";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Question from "./questions/Question";
import Answer from "./answers/Answers";
import Answer1 from "./answers/Answer1";
import "./ContractModeling.css";
import Axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
}));

export default function ContractModeling(props) {
  const [questions, setQuestions] = React.useState();
  const [answers1, setAnswers1] = React.useState();
  const capabilityID = props.match.params.capabilityID;
  const url =
    "http://localhost:8080/api/questions?capabilityID=" + capabilityID;
  const toSubmit = useSelector((state) => state.addQAReducer);
  console.log(toSubmit);
  useEffect(() => {
    Axios.get(url).then((response) => {
      console.log(response.data);
      const data = response.data;
      const row = data.map((el) => {
        return {
          [el.questionID]: el.questionText,
        };
      });
      const answersRow = data.map((el) => {
        console.log(el.answers);
        return el.answers.map((e) => {
          return {
            qid: el.questionID,
            id: e.answerID,
            text: e.answerText,
          };
        });
      });
      setQuestions(row);
      console.log(answersRow);
      setAnswers1(answersRow);
    });
  }, []);

  const classes = useStyles();
  /*const questions = {
    1: "How do you describe your API to the consumers?",
    2: "What approach is followed by enterprise to create API contract or interface?",
    3: "What tools are being used to design an API?",
    4: "Which API specification is followed for designing API interface or contract?",
    5: "How do you gain feedback from API consumer about your API contract?",
    6: "What strategy or approach you follow for API versioning?",
    7: "When do you decide to go for a new major version?",
    8: "What scenerios do you consider for backward breaking chnages in API?",
    9: "What purpose are served by API contract in your organization?",
  };*/
  /*const answers = {
    1: {
      1: "API Contract",
      2: "Microsoft document",
      3: "Other means",
    },
    2: {
      1: "Design First",
      2: "Code First",
    },
    3: {
      1: "Swagger UI",
      2: "API Lifecyle Tool ( eg: Anypoint design center)",
      3: "Microsoft Excel",
      4: "Visio",
    },
    4: {
      1: "RAML",
      2: "Open API 3.0",
      3: "Multiple specification followed",
    },
    5: {
      1: "During the API design using Mocking",
      2: "After API is deployed in Test environment",
      3: "After API deployed in Production",
    },
    6: {
      1: "Semantic Versioning ",
      2: "Some are following Semantic versioning and some no strategy",
      3: "No strategy for versioning",
    },
    7: {
      1: "Breaking backward compatibility",
      2: "For any changes in API",
    },
    8: {
      1: "Change in payload structure",
      2: "Change in Resource URL",
      3: "Change in HTTP methods",
      4: "All of the above",
    },
    9: {
      1: "To generate Resource beans for Developing API",
      2: "API consumer can start their development based on the API contract",
      3: "To generate Mock of API",
    },
  };*/
  const weight = {
    1: {
      1: 10,
      2: 15,
      3: 20,
    },
    2: {
      1: 10,
      2: 20,
    },
    3: {
      1: 10,
      2: 15,
      3: 20,
      4: 25,
    },
    4: {
      1: 10,
      2: 15,
      3: 20,
    },
    5: {
      1: 10,
      2: 15,
      3: 20,
    },
    6: {
      1: 10,
      2: 15,
      3: 20,
    },
    7: {
      1: 10,
      2: 15,
    },
    8: {
      1: 10,
      2: 15,
      3: 20,
      4: 25,
    },
    9: {
      1: 10,
      2: 15,
      3: 20,
    },
  };
  //const [questions, setQuestions] = React.useState(ques);
  //const [answers, setAnswers] = React.useState(ans);
  //const [weight, setWeight] = React.useState(scoreWeight);
  const dispatch = useDispatch();
  const [clickedAnswer, setClickedAnswer] = React.useState([]);
  const [step, setStep] = React.useState(1);
  const [score, setScore] = React.useState(0);
  //console.log(questions);

  const checkAnswer = (answer) => {
    const arr = weight[step];
    //console.log(arr[answer]);
    setScore(score + arr[answer]);
    setClickedAnswer((a) => [...a, answer]);
    console.log(clickedAnswer);
  };

  const nextStep = (step) => {
    const ques = questions[step - 1][step];
    dispatch(addQandA(step, clickedAnswer));
    setStep(step + 1);
    setClickedAnswer([]);
  };

  const submit = () => {
    dispatch(setContractModelingScore(score));
    props.history.push("/");
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute">
        <h2 className="text-center">IBM API Maturity Assessment</h2>
      </AppBar>
      <main className={classes.content}>
        {questions && step <= Object.keys(questions).length ? (
          <div className="Content">
            <Question question={questions[step - 1][step]} />

            {answers1 && (
              <Answer1
                answer={answers1[step - 1]}
                step={step}
                checkAnswer={checkAnswer}
                clickedAnswer={clickedAnswer}
              />
            )}

            <button
              className="NextStep"
              disabled={
                clickedAnswer && Object.keys(questions).length >= step
                  ? false
                  : true
              }
              onClick={() => nextStep(step)}
            >
              Next
            </button>
          </div>
        ) : (
          <div className="finalPage">
            <h1>You have completed the Survey!</h1>
            <p>Capability score is: {score}</p>
            <p>Thank you!</p>
            <button className="NextStep" onClick={submit}>
              Submit
            </button>
          </div>
        )}
      </main>
    </div>
  );
}
