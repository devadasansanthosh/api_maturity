import React from "react";
import "./Answers.css";

const Answer1 = (props) => {
  const step = props.step;
  const answers = props.answer;
  const qid = props.answer[0].qid;
  console.log(props.clickedAnswer);
  return (
    <>
      <ul className="Answers">
        {answers.map((el) => (
          <li
            key={el.id}
            className={
              props.clickedAnswer !== 0 && props.clickedAnswer.includes(el.id)
                ? "clicked"
                : ""
            }
            onClick={() => props.checkAnswer(el.id)}
          >
            {el.text}
          </li>
        ))}
      </ul>
    </>
  );
};
export default Answer1;
