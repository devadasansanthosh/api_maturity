import React from "react";
import "./Answers.css";

const Answers = (props) => {
  let answers = Object.keys(props.answer).map((qAnswer, i) => (
    <li
      className={props.clickedAnswer === qAnswer ? "clicked" : ""}
      onClick={() => props.checkAnswer(qAnswer)}
      key={qAnswer}
    >
      {props.answer[qAnswer]}
    </li>
  ));
  return (
    <>
      <ul disabled={props.clickedAnswer ? true : false} className="Answers">
        {answers}
      </ul>
    </>
  );
};
export default Answers;
